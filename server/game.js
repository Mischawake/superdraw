const _ = require('lodash');
const assert = require('assert');
const now = require('performance-now');
const THREE = require('three');

const Helpers = require('./serverhelpers').Helpers;

let PerlinGenerator = require("proc-noise");
let Perlin = new PerlinGenerator();

const kTickRate = 60;

const MAX_PLAYERS = 2;

const MIN_SPEED = 0.1;
const MAX_SPEED = 50.0;
const MIN_SPIN = -1.5;
const MAX_SPIN = 1.5;
const MIN_DURATION = 1.0;
const MAX_DURATION = 30.0;
const MAX_REFLECTIONS = 99;
const MIN_REFLECTIONS = 1;
const RANGE_MULT = 0.33;

const ACTIVATION_DIST = 4.0;
const CONNECTION_TIMEOUT = 3000.0;

const MAX_GAMES = 200; //400 players

//klido.io
//kaleidoscop.io

//
// Player
//
class Player {
    constructor(game, ws, id) {
        
        this.id = id;
        this.ws = ws;
        this.heartbeat = now();
        this.ready = false;

        this.netState = 0;

        ws.binaryType = "arraybuffer"; 

        ws.on('message', (data) => {
            this.heartbeat = now();

            let recvMsg = (m) => {
                if (typeof(m) === 'object' ){
                    return {
                        type: 'binary',
                        data: data
                    };
                }        
                else if (typeof(m) === 'string') {
                    return JSON.parse(m);
                } else {
                    return {
                        type: 'unknown type: ' + typeof(data),
                        data: data
                    };
                }
            };

            let msg = recvMsg(data);
            game.onMessage(this, msg.type, msg.data);

        });

        ws.on('error', (err) => {
            console.log('Error', err);
        });

        ws.on('close', () => {
            game.removePlayer(this);
            this.ws = null;
        });
    }

    getDrawing(){
        return ( this.netState & 0x1 ) === 1;
    }

    getReflects(){
        return ( this.netState & 0xFE ) >>> 1;
    }

    getPosition(){

        let y = Helpers.decompact((this.netState & 0xFFF00 ) >>> 8, -100, 100, 0xFFF );
        let x = Helpers.decompact((this.netState & 0xFFF00000 ) >>> 20, -100, 100, 0xFFF);
        return new THREE.Vector2(x,-y);
      
    }

    sendMessage(type, data) {

        let msg;

        if( type === "binary" ){
            msg = data;
        }
        else{
            msg = JSON.stringify({
                type: type,
                data: data
            });
        }

        this.ws.send(msg, (err) => {
            if (err !== undefined) {
                //console.log('Send error', err);
                console.log("Send Failed, closing connection");
                this.ws.close();
            }
        });
    }
}

//
// Game
//
//some outstanding issue with player ids?


class Mood{

    constructor(){

        this.pointer = Math.random()*1000.0;
        this.speed = 0.0;
        this.spin = 0.0;
        this.duration = 0.0;

        this.nextMood();
    }

    nextMood(){

        this.pointer += 2.0;

        this.speed = MIN_SPEED + Math.abs( Perlin.noise(0,this.pointer) - 0.5 ) * 2.0 * (MAX_SPEED - MIN_SPEED);
        this.spin = MIN_SPIN + Perlin.noise(100,this.pointer) * (MAX_SPIN - MIN_SPIN);
        this.duration = MIN_DURATION + Math.pow(Perlin.noise(200,this.pointer),2) * (MAX_DURATION - MIN_DURATION);

        //console.log( "Speed:"+this.speed+" Spin:"+this.spin+" Duration:"+this.duration );
    }





}


let bonusTheta = 0;
let nonFancyBonusCount = 0;

class Bonus{

    /*
    Types:
    Player Color 0
    Player Reflections 1
    World Speed 2
    World Spin 3
    World Duration 4
    */

    constructor( spin, speed, duration, minReflect, maxReflect ){

        this.id = Math.floor( Math.random() * 10000 );
        this.duration = 10.0;

        //this.bonus_type = bonusPointer;
        this.bonus_value = 0;

        if( nonFancyBonusCount >= 4 ){

            this.bonus_type = 2;
            nonFancyBonusCount = 0;
        }
        else{
            if( Math.random() > 0.5 ){
                this.bonus_type = 0;
            }
            else{
                this.bonus_type = 1;
            }
            nonFancyBonusCount++;
        }

        switch (this.bonus_type) {
        case 0:{
            //player color
            this.bonus_value = Math.random();
            break;
        }
        case 1:{
            //player reflections 
            let range = 10;
            let low = Math.max( maxReflect - range, MIN_REFLECTIONS);
            let high = Math.min( minReflect + range, MAX_REFLECTIONS);
            this.bonus_value = Math.round( low + Math.random() * ( high - low));
            break;
        }
        case 2:{
            //mood
            //this.bonus_value = Math.random(); // controls color only
            break;
        }
        default:
            console.log('Unknown bonus type');
        }

        bonusTheta += Math.random() * Math.PI + Math.PI / 8;
        let rad = 40;
        this.position = {x: Math.sin(bonusTheta)*rad,y: Math.cos(bonusTheta) * rad };
        this.lifetime = 0;
    }

    update(dt){
        this.lifetime+=dt;
    }

}

class Game {

    constructor(id) {
        this.id = id;
        this.players = [];
        this.timeSinceUpdate = 0;
        this.timeSinceStatus = 0;
        this.started = true;
        this.activeIds = new Set(); //seems like a good feature, but not sure if necessary?

        this.timeToBonus = 10.0;

        this.bonuses = [];

        //these could be randomized
        this.spin = 0;
        this.speed = 5.0; 
        this.duration = 5.0;

        this.mood = new Mood();

    }

    onConnection(ws) {

        let id = 0;
        while( this.activeIds.has(id) ){
            id++;
        }

        this.activeIds.add( id );
        let p = new Player(this, ws, id);
        this.players.push(p);
        //console.log("Player joined Game:"+this.id+" w/ ID:"+p.id);

        //moved from
        p.sendMessage('joined', {
            playerId: p.id
        });

        //sync initial state!
        this.broadcast('mood', {
            spin: this.mood.spin,
            speed: this.mood.speed,
            duration: this.mood.duration
        });
    }

    onMessage(player, type, data) {
        switch (type) {
        case 'ready':
            //console.log('Player ready', this.id, player.id);
            player.ready = true;
            break;
        case 'binary':
            this.deserialize( player, data );
            break;
        default:
            console.log('Unknown message', this.id, player.id, type, data);
        }
    }

    removePlayer(player) {
        //console.log('Player left', this.id, player.id);
        this.activeIds.delete( player.id );
        this.players.splice(this.players.indexOf(player), 1);
    }

    isEmpty(){
        return this.players.length == 0;
    }

    broadcast(type, data) {

        for (let i = 0; i < this.players.length; i++) {
            let p = this.players[i];
            p.sendMessage(type, data);
        }
    }

    deserialize( player, data ){

        let dataView = new DataView(data);
        player.netState = dataView.getUint32( 0 ); 

    }

    update(dt,currentPlayers) {
        
        if (!this.started || this.isEmpty() ) {
            return;
        }

        this.speed = Helpers.clamp( this.speed, MIN_SPEED, MAX_SPEED );
        this.spin = Helpers.clamp( this.spin, MIN_SPIN, MAX_SPIN );
        this.duration = Helpers.clamp( this.duration, MIN_DURATION, MAX_DURATION );

        this.timeToBonus -= dt;
        this.timeSinceUpdate += dt;
        this.timeSinceStatus += dt;

        // for( let i = this.players.length - 1; i >= 0; i--){
              
        //     if( now() - this.players[i].heartbeat > CONNECTION_TIMEOUT ){
        //         this.players[i].ws.close();
        //     }
        // }


        for( let i = this.bonuses.length-1; i >= 0; i-- ){
            this.bonuses[i].update(dt);

            let activate = -1;
            for( let j = 0; j < this.players.length; j++ ){

                let p = this.players[j].getPosition();
                //let d = this.players[j].getDrawing();

                if( p.distanceTo(new THREE.Vector2(this.bonuses[i].position.x,this.bonuses[i].position.y) ) < ACTIVATION_DIST ){
                    activate = this.players[j].id;
                }
            }


            if( this.bonuses[i].lifetime > this.bonuses[i].duration || activate >= 0){
                
                this.broadcast('bonus', {
                    create: false,
                    activated: activate,
                    id: this.bonuses[i].id
                });

                if( activate >= 0 ){

                    let b = this.bonuses[i];

                    if( b.bonus_type == 2){

                        //mood
                        this.mood.nextMood();
                        this.broadcast('mood', {
                            spin: this.mood.spin,
                            speed: this.mood.speed,
                            duration: this.mood.duration
                        });
                    }
                    else{

                        this.broadcast('effect', {
                            playerId: activate,
                            bonus_type: b.bonus_type,
                            bonus_value: b.bonus_value
                        });

                    }

                   

                 

                }

                this.bonuses.splice(i,1);
            }
        }




        if( this.timeToBonus <= 0 && this.bonuses.length < 3 ){

            this.timeToBonus = 10.0;

            let minReflect = 99;
            let maxReflect = 1;

            for( let j = 0; j < this.players.length; j++ ){

                minReflect = Math.min( this.players[j].getReflects() );
                maxReflect = Math.max( this.players[j].getReflects() );

            }

            let b = new Bonus( this.spin, this.speed, this.duration, minReflect, maxReflect );
            
            this.bonuses.push( b );

            //let ref = Math.floor(1.0 + Math.random() * 100 );
            //console.log( "new Bonus " + b.id + " bonus count:" + this.bonuses.length);

            // generate bonus here
            this.broadcast('bonus', {
                create: true,
                id: b.id,
                duration: b.duration,
                bonus_type: b.bonus_type,
                bonus_value: b.bonus_value,
                position: b.position
            });

        }

        if( this.timeSinceUpdate > 1.0 / 30.0 ){

            this.timeSinceUpdate = 0.0;

            let numPlayers = this.players.length;

            let arrayBuffer = new ArrayBuffer( 2 + numPlayers * 6 ); 
            let dataView = new DataView(arrayBuffer);
            let offset = 0;

            dataView.setUint16( offset, numPlayers );
            offset += 2;

            for( let i = 0; i < this.players.length; i++ ){
                 
                let p = this.players[i];
                dataView.setUint16(offset, p.id ); 
                offset += 2;
                dataView.setUint32(offset, p.netState ); 
                offset += 4;
            
            }

            this.broadcast('binary',arrayBuffer);

        }

        if( this.timeSinceStatus > 1.0 ){

            this.timeSinceStatus = 0.0;

            this.broadcast('status', {
                currentPlayers: currentPlayers
            });

        }
   
    }

}

//
// GameManager
//

let timeSinceProfile = 0.0;
let timeSinceCount = 0.0;
let frameCount = 0;

class GameManager {
    constructor() {

        this.games = [];
        this.gameId = 0;
        this.prevTime = now();
        this.cullTime = 0;

        this.totalPlayerCount = 0;
  

        setInterval(() => { this.update(); }, 1000.0 / kTickRate);
    }

    onConnection(ws) {
        //look for game with opening in it, not just most recent game!
        let game = null;

        for( let i = 0; i < this.games.length; i++ ){

            //find an open game
            let game = this.games[i];
            if( game.players.length < MAX_PLAYERS ){
                game.onConnection(ws);
                return;
            }
        }

        if( this.games.length == MAX_GAMES ){ 
            //no open games
            ws.close();
            return;
        }

        //if failed, create a new game
        game = new Game(this.gameId++);
        this.games.push(game);
        game.onConnection(ws);
        
    }

    update() {
        let time = now();
        let dt = Math.min((time - this.prevTime) / 1000.0, 1.0 / 8.0);
        this.prevTime = time;

        this.cullTime += dt;

        frameCount++;
        timeSinceProfile+=dt;
        timeSinceCount+=dt;


        if( timeSinceCount > 1.0 ){
            this.totalPlayerCount = 0;
            for (let i = 0; i < this.games.length; i++) {
                let g = this.games[i];
                this.totalPlayerCount += g.players.length;
            }
            timeSinceCount = 0;
        }

        if( timeSinceProfile > 10.0 ){

            console.log( "--------------------------------");
            console.log( "Average FPS:"+frameCount/timeSinceProfile);
            console.log( "Total Players:"+ this.totalPlayerCount );
            console.log( "Total Games:"+ this.games.length );

            timeSinceProfile = 0;
            frameCount = 0;
        }

        for (let i = this.games.length - 1; i >= 0; i--) {
           
            let g = this.games[i];
            g.update(dt,this.totalPlayerCount);

            if (g.isEmpty()) {
                console.log("Closing out empty game id:"+g.id );
                this.games.splice(i, 1);
            }
        }

        if( this.cullTime > 1.0 ){
            for (let i = this.games.length - 1; i >= 0; i--) {   
                let g = this.games[i];
                if( g.players.length == 1 ){    
                    let p = g.players[0];
                    for( let j = 0; j < this.games.length; j++ ){
                        let targetGame = this.games[j];
                        if( targetGame.players.length == 1 && j != i ){
                            // game with one player available
                            console.log("Combining games" + g.id + " and " + targetGame.id );
                            g.removePlayer(p);
                            targetGame.onConnection(p.ws);
                            break;
                        }
                    }
                }
            }
            this.cullTime = 0;
        }

    }
}

module.exports = {
    Game,
    GameManager
};
