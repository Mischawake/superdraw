require('console-stamp')(console, {pattern: 'yyyy-mm-dd HH:MM:ss.L'});
const express = require('express');
const http = require('http');
const https = require('https');
const url = require('url');
const WebSocket = require('ws');
const Game = require('./game.js');

const SSL = false;

//EXPRESS APP
let io_app = express();
let dir = __dirname + '/../app';
//console.log('Serving static files from', dir);
io_app.use(express.static(dir));

//
// Server
//

//Must be updated for each site...
let credential_email = 'dev@fwmf.michaelhighland.com';
let credential_site = 'fwmf.michaelhighland.com';

if( process.env.NODE_ENV == 'production' && SSL )
{

    //// SETUP HTTP GREENLOCK

    let greenlock = require('greenlock-express').create({

          // Let's Encrypt v2 is ACME draft 11
        version: 'draft-11'

        , server: 'https://acme-v02.api.letsencrypt.org/directory'
          // Note: If at first you don't succeed, switch to staging to debug
          // https://acme-staging-v02.api.letsencrypt.org/directory

          // You MUST change this to a valid email address
        , email: credential_email

          // You MUST NOT build clients that accept the ToS without asking the user
        , agreeTos: true

          // You MUST change these to valid domains
          // NOTE: all domains will validated and listed on the certificate
        , approveDomains: [ credential_site, 'www.'+credential_site ]

          // You MUST have access to write to directory where certs are saved
          // ex: /home/foouser/acme/etc
        , configDir: require('path').join(require('os').homedir(), 'acme', 'etc')

        //, app: io_app

          // Join the community to get notified of important updates and help me make greenlock better
        , communityMember: true

          // Contribute telemetry data to the project
        , telemetry: true

        , debug: true

    });

    //// REDIRECT HTTP TO HTTPS

    let redirectHttps = require('redirect-https')();
    let acmeChallengeHandler = greenlock.middleware(redirectHttps);
    http.createServer(acmeChallengeHandler).listen(80, function () {
        console.log("Listening for ACME http-01 challenges on", this.address());
    });

    //// HTTPS SERVER + WEBSOCKETS

    let server = https.createServer(greenlock.tlsOptions, io_app); 

    let ws = new WebSocket.Server({server});

    let gameManager = new Game.GameManager();

    ws.on('connection', function(ws, req) {
        let loc = url.parse(req.url, true).path;

        ws.name = req.connection.remoteAddress + ':' + req.connection.remotePort;
        //console.log('Connected %s to %s', ws.name, loc);

        if (loc == '/play') {
            gameManager.onConnection(ws);
        }
    });

    server.listen(443);

}
else{
    //ORGINAL WAY OF DOING IT ALL...NO HTTPS

    let server = http.createServer(io_app);

    let ws = new WebSocket.Server({ server });

    let gameManager = new Game.GameManager();

    ws.on('connection', function(ws, req) {
        let loc = url.parse(req.url, true).path;

        ws.name = req.connection.remoteAddress + ':' + req.connection.remotePort;
        //console.log('Connected %s to %s', ws.name, loc);

        if (loc == '/play') {
            gameManager.onConnection(ws);
        }
    });

    const kPort = process.env.NODE_ENV == 'production' ? 80 : 8080;
    server.listen(kPort, function() {
        console.log('Server listening on port %d', server.address().port);
    });
}


