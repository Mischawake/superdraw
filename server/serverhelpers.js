const THREE = require('three');

class Helpers{

    static clamp(v, min, max) {
        return Math.min(Math.max(v, min), max);
    }

    static lerp(t, a, b) {
        return a * (1.0 - t) + b * t;
    }

    static remap(t, a, b, a1, b1) {
        let x = this.clamp(t, a, b);
        return this.lerp(this.delerp(x, a, b), a1, b1);
    }

    static meet( max, a, b ){
        return a + this.clamp(b - a, -max, max );
    }

    static pilerp(t, a, b) {
        a = this.modpi( a );
        b = this.modpi( b );
        if( Math.abs( a - b ) > Math.PI ){
            if( a < 0 ) a += Math.PI * 2.0;
            else b += Math.PI * 2.0;
        }
        return this.modpi( this.lerp( t, a, b ) );
    }

    static modpi( v ) {
        v = v % ( Math.PI * 2 );
        if( v > Math.PI ) v -= Math.PI * 2;
        if( v < -Math.PI ) v += Math.PI * 2;
        return v;
    }

    static delerp(t, a, b) {
        if( a == b ) return 0;
        let max = Math.max( a, b );
        let min = Math.min( a, b );
        return (t - min) / (max - min);
    }

    static lerpVector3(t, a, b) {
        let r = new THREE.Vector3();
        r.x = this.lerp(t, a.x, b.x);
        r.y = this.lerp(t, a.y, b.y);
        r.z = this.lerp(t, a.z, b.z);
        return r;
    }

    static compress16( dataView, offset, value, range )
    {
        value = this.clamp( value, 0, range );
        dataView.setUint16( offset, Math.floor( value / range * 0xFFFF) ); 
    }

    static decompress16( dataView, offset, range )
    {
        let value = dataView.getUint16( offset );
        value /= 0xFFFF;
        value *= range;
        return value;
    }

    static compact( value, min, max, compactor ){

        value = this.clamp( value, min, max );
        let range = max - min;
        return Math.floor( ( value + range/2.0 ) / range * compactor );
    }

    static decompact( value, min, max, compactor ){

        let range = max - min;
        value *= range / compactor;
        value -= range/2.0;
        return value;
    }

    static randomUnitVector3(){

        return new THREE.Vector3( Math.random() * 2 - 1, Math.random() * 2 - 1, Math.random() * 2 - 1 ).normalize();
    }

    static randomUnitVector2(){

        return new THREE.Vector2( Math.random() * 2 - 1, Math.random() * 2 - 1 ).normalize();
    }

    static randomAbsRange( range )
    {
        return (Math.random() * 2 - 1) * range / 2;
    }

    // Taken from http://libnoise.sourceforge.net
    // Modified to a 0 - 1 range
    static intNoise(n) {
        n = (n >> 13) ^ n;
        let nn = (n * (n * n * 60493 + 19990303) + 1376312589) & 0x7fffffff;
        return (nn / 1073741824.0) * 0.5;
    }

}

module.exports = {
    Helpers
};





