const MAX_LINES = 128000;
const MAX_TRIS = 16000;

const THREE = require('three');
const DebugHud = require('./debug').debugHud;

class GeoBarn{

    constructor( scene ){
        {
            let tris = MAX_TRIS;
            this.geometryBuffer = new THREE.BufferGeometry();
            this.geometryBuffer.dynamic = true;
            let positions = new Float32Array( tris * 3 * 3 );
            let colors = new Float32Array( tris * 3 * 3 );
            let normals = new Float32Array( tris * 3 * 3 );

            this.geometryBuffer.addAttribute('position', new THREE.BufferAttribute(positions, 3));
            this.geometryBuffer.addAttribute('color', new THREE.BufferAttribute(colors, 3));
            this.geometryBuffer.addAttribute('normal', new THREE.BufferAttribute(normals, 3));
          
            let material = new THREE.MeshBasicMaterial({ vertexColors: THREE.VertexColors });

            // let material = new THREE.MeshPhongMaterial({
            //  vertexColors: THREE.VertexColors,
            //     specular: new THREE.Color(0.05, 0.3, 0.1),
            //     shininess: 0,
            //     shading: THREE.FlatShading
            //         });

            this.mesh = new THREE.Mesh(this.geometryBuffer, material);
            scene.add(this.mesh);
            this.idxPointer = 0;
        }

        {
            this.lineBuffer = new THREE.BufferGeometry();
            this.lineBuffer.dynamic = true;
            let positions = new Float32Array( MAX_LINES * 2 * 3 );
            let colors = new Float32Array( MAX_LINES * 2 * 3 );
            this.lineBuffer.addAttribute('position', new THREE.BufferAttribute(positions, 3));
            this.lineBuffer.addAttribute('color', new THREE.BufferAttribute(colors, 3));
            let material = new THREE.LineBasicMaterial({ vertexColors: THREE.VertexColors });
            this.lineMesh = new THREE.LineSegments( this.lineBuffer, material );
            scene.add(this.lineMesh);
            this.lineIdxPointer = 0;
        }

    }

    addLine(a, b, aColor, bColor )
    {
        this.addLineVertex( a, aColor );
        this.addLineVertex( b, bColor );
    }

    //experimenting with kaliedoscopics
    addLineK(a, b, aColor, bColor, reflections )
    {

        for( let i = 0; i <= reflections; i++ ){

            let p0 = a.clone();
            let p1 = b.clone();

            let angle = Math.PI * 2.0 / reflections * i;

            // let s = Math.sin(angle);
            // let c = Math.cos(angle);

            // // rotate point
            // p0.x = p0.x * c - p0.z * s;
            // p0.z = p0.x * s + p0.z * c;

            // p1.x = p1.x * c - p1.z * s;
            // p1.z = p1.x * s + p1.z * c;

            p0.applyAxisAngle( new THREE.Vector3(0,1,0), angle );
            p1.applyAxisAngle( new THREE.Vector3(0,1,0), angle );

            this.addLineVertex( p0, aColor );
            this.addLineVertex( p1, bColor );

        }


    }

    addLineVertex(v, color) 
    {
        let colors = this.lineBuffer.attributes.color.array;
        let positions = this.lineBuffer.attributes.position.array;

        if( this.lineIdxPointer >= MAX_LINES * 2 * 3 ) return; // all filled

        positions[ this.lineIdxPointer ] = v.x;
        positions[ this.lineIdxPointer + 1 ] = v.y;
        positions[ this.lineIdxPointer + 2 ] = v.z;

        colors[ this.lineIdxPointer ] = color.r;
        colors[ this.lineIdxPointer + 1 ] = color.g;
        colors[ this.lineIdxPointer + 2 ] = color.b;
        
        this.lineIdxPointer += 3;
    }

    addVert( pos, color, normal )
    {
        let colors = this.geometryBuffer.attributes.color.array;
        let positions = this.geometryBuffer.attributes.position.array;
        let normals = this.geometryBuffer.attributes.normal.array;

        if( this.idxPointer >= MAX_TRIS * 3 * 3 ) return; // all filled

        positions[ this.idxPointer ] = pos.x;
        positions[ this.idxPointer + 1 ] = pos.y;
        positions[ this.idxPointer + 2 ] = pos.z;

        colors[ this.idxPointer ] = color.r;
        colors[ this.idxPointer + 1 ] = color.g;
        colors[ this.idxPointer + 2 ] = color.b;

        normals[ this.idxPointer ] = normal.x;
        normals[ this.idxPointer + 1 ] = normal.y; //up i think
        normals[ this.idxPointer + 2 ] = normal.z;

        this.idxPointer += 3;

    }

    drawBird( worldPos, scale, angle, color, filled = false ){

        var rotMatrix = new THREE.Matrix3();
        let cos = Math.cos( angle );
        let sin = Math.sin( angle );

        rotMatrix.set(  cos, 0, sin,
                        0, 1, 0,
                       -1 * sin, 0, cos );

        let points = [];

        points.push( new THREE.Vector3( 0,  0, -1.414 ) );

        let theta = Math.PI / 4.0;
        for( let i = 0; i <= 18; i++ ){
            points.push( new THREE.Vector3( Math.sin( theta ),  0, -Math.cos( theta )  ) );
            theta += Math.PI * 1.5 / 18.0;
        }
        for( let i = 0; i < points.length; i++ ){

            points[i].multiplyScalar( scale );
            points[i].applyMatrix3( rotMatrix );
        }

        let a = new THREE.Vector3();
        let b = new THREE.Vector3();
        let n = new THREE.Vector3(0,1,0);

        for( let i = 0; i < points.length - 1; i++ ){

            a.addVectors(points[i],worldPos);
            b.addVectors(points[i+1],worldPos);
            
            if( filled ){
                this.addVert( b, color, n);
                this.addVert( a, color, n);
                this.addVert( worldPos, color, n);
            }
            else{
                this.addLine( a, b, color, color );
            }
        
        }

        a.addVectors(points[points.length-1],worldPos);
        b.addVectors(points[0],worldPos);

        if( filled ){

            this.addVert( b, color, n);
            this.addVert( a, color, n);
            this.addVert( worldPos, color, n);

        }
        else{
            this.addLine( a, b, color, color );
        }

        

    }

    drawFilledRect( pos, height, width, color ){

        let n = new THREE.Vector3(0,1,0);

        let p0 = new THREE.Vector3(pos.x - width / 2.0, pos.y, pos.z + height / 2.0);
        let p1 = new THREE.Vector3(pos.x + width / 2.0, pos.y, pos.z + height / 2.0);
        let p2 = new THREE.Vector3(pos.x + width / 2.0, pos.y, pos.z - height / 2.0);
        let p3 = new THREE.Vector3(pos.x - width / 2.0, pos.y, pos.z - height / 2.0);

        this.addVert(p0, color, n);
        this.addVert(p1, color, n);
        this.addVert(p3, color, n);

        this.addVert(p3, color, n);
        this.addVert(p1, color, n);
        this.addVert(p2, color, n);


    }

    drawRect( pos, height, width, color ){

        let p0 = new THREE.Vector3(pos.x - width / 2.0, pos.y, pos.z + height / 2.0);
        let p1 = new THREE.Vector3(pos.x + width / 2.0, pos.y, pos.z + height / 2.0);
        let p2 = new THREE.Vector3(pos.x + width / 2.0, pos.y, pos.z - height / 2.0);
        let p3 = new THREE.Vector3(pos.x - width / 2.0, pos.y, pos.z - height / 2.0);

        this.addLine( p0, p3, color, color );
        this.addLine( p3, p2, color, color );
        this.addLine( p2, p1, color, color );
        this.addLine( p1, p0, color, color );
    }

    drawCircle( center, radius, color, segments = 18, rad_offset = 0, stroked = false )
    {
    
        let lastPoint = new THREE.Vector3();
        let newPoint = new THREE.Vector3();
    
        let offset = new THREE.Vector3();

        for( let i = 0; i <= segments; i++ )
        {   
            let theta = rad_offset + Math.PI * 2 / segments * i;
            offset.set( Math.sin(theta), 0, Math.cos(theta));
            offset.multiplyScalar( radius );
            newPoint.addVectors( center, offset );
            
            if( i > 0 ){
                this.addLine( lastPoint, newPoint, color, color );
                if( stroked ) this.addLine( center, newPoint, color, color );
            } 

            lastPoint.copy( newPoint );
        }
    }

    drawFilledCircle( center, radius, color, segments = 18, rad_offset = 0 )
    {
    
        let lastPoint = new THREE.Vector3();
        let newPoint = new THREE.Vector3();
    
        let n = new THREE.Vector3(0,1,0);
        // let up = new THREE.Vector3( 0, 0, 1 );
        // let right = new THREE.Vector3( 1, 0, 0 );

        let offset = new THREE.Vector3();

        for( let i = 0; i <= segments; i++ )
        {   
            let theta = rad_offset + Math.PI * 2 / segments * i;
            offset.set( Math.sin(theta), 0, Math.cos(theta));
            offset.multiplyScalar( radius );
            newPoint.addVectors( center, offset );

            if( i > 0 ){

                this.addVert( lastPoint, color, n);
                this.addVert( newPoint, color, n);
                this.addVert( center, color, n);
            } 
            
            lastPoint.copy( newPoint );

        }

    }

    drawSpiral( center, radius, swirl, color, segments = 18 ){

        let lastPoint = new THREE.Vector3();
        let newPoint = new THREE.Vector3();
    
        let offset = new THREE.Vector3();

        let pieces = Math.floor( segments * swirl / (Math.PI * 2) );

        for( let i = 0; i <= pieces; i++ )
        {   
            let theta = i/pieces * swirl;
            offset.set( Math.sin(theta), 0, Math.cos(theta));
            offset.multiplyScalar( i / pieces * radius );
            newPoint.addVectors( center, offset );
            
            if( i > 0 ) this.addLine( lastPoint, newPoint, color, color );
            lastPoint.copy( newPoint );
        }


    }

    drawArc( center, radius, color, start, end, segments = 18 )
    {
    
        let lastPoint = new THREE.Vector3();
        let newPoint = new THREE.Vector3();
        let offset = new THREE.Vector3();

        let theta = start;

        while( theta <= end )
        {   
            offset.set( Math.sin(theta), 0, Math.cos(theta));
            offset.multiplyScalar( radius );
            newPoint.addVectors( center, offset );
            
            if( theta != start ) this.addLine( lastPoint, newPoint, color, color );
            lastPoint.copy( newPoint );

            theta += Math.PI * 2 / segments;
        }
    }



    flush(){

        this.geometryBuffer.attributes.position.needsUpdate = true;
        this.geometryBuffer.attributes.color.needsUpdate = true;
        this.geometryBuffer.attributes.normal.needsUpdate = true;
        this.geometryBuffer.attributes.position.count = this.idxPointer / 3 ;
        this.geometryBuffer.attributes.color.count = this.idxPointer / 3;
        this.geometryBuffer.attributes.normal.count = this.idxPointer / 3;
        this.mesh.frustumCulled = false; 
    

        this.lineBuffer.attributes.position.needsUpdate = true;
        this.lineBuffer.attributes.color.needsUpdate = true;
        this.lineBuffer.attributes.position.count = this.lineIdxPointer / 3 ;
        this.lineBuffer.attributes.color.count = this.lineIdxPointer / 3;
        this.lineMesh.frustumCulled = false;
        
        DebugHud.addLine("Line-Verts:" + this.lineIdxPointer / 3 );

        //console.log( "Tri-Vert:"+this.idxPointer / 3+" Line-Verts:" + this.lineIdxPointer / 3 );
        
        this.lineIdxPointer = 0;
        this.idxPointer = 0;
    }

}


class Seg16 {

    constructor( geoBarn ){

        this.geoBarn = geoBarn;

        this.w = 0.75;
        this.h= -1.0;
        this.z = 0.0;
        this.p0 = new THREE.Vector3(0.0,this.z,this.h);
        this.p1 = new THREE.Vector3(this.w/2.0,this.z,this.h);
        this.p2 = new THREE.Vector3(this.w,this.z,this.h);
        this.p3 = new THREE.Vector3(this.w,this.z,this.h/2.0);
        this.p4 = new THREE.Vector3(this.w,this.z,0.0);
        this.p5 = new THREE.Vector3(this.w/2.0,this.z,0.0);
        this.p6 = new THREE.Vector3(0.0,this.z,0.0);
        this.p7 = new THREE.Vector3(0.0,this.z,this.h/2.0);
        this.p8 = new THREE.Vector3(this.w/2.0,this.z,this.h/2.0);

        this.charCodelookup = new Array(255);

        for( let i = 0; i < 255; i++ ){

            this.charCodelookup[i] = new Array(17);
            this.charCodelookup[i].fill(0);

            for( let j = 0; j < 16; j++ ){
                this.charCodelookup[i][j] = (Math.random() < 0.25 ? 1 : 0);
            }   
        }

        this.charCodelookup[32] = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0];//Space

        this.charCodelookup[48] = [1,1,1,1, 1,1,1,1, 0,0,1,0, 0,0,1,0, 0];//0
        this.charCodelookup[49] = [0,0,1,1, 0,0,0,0, 0,0,1,0, 0,0,0,0, 0];//1
        this.charCodelookup[50] = [1,1,1,0, 1,1,1,0, 0,0,0,1, 0,0,0,1, 0];//2
        this.charCodelookup[51] = [1,1,1,1, 1,1,0,0, 0,0,0,1, 0,0,0,1, 0];//3
        this.charCodelookup[52] = [0,0,1,1, 0,0,0,1, 0,0,0,1, 0,0,0,1, 0];//4
        this.charCodelookup[53] = [1,1,0,1, 1,1,0,1, 0,0,0,1, 0,0,0,1, 0];//5
        this.charCodelookup[54] = [1,1,0,1, 1,1,1,1, 0,0,0,1, 0,0,0,1, 0];//6
        this.charCodelookup[55] = [1,1,0,0, 0,0,0,0, 0,0,1,0, 0,0,1,0, 0];//7
        this.charCodelookup[56] = [1,1,1,1, 1,1,1,1, 0,0,0,1, 0,0,0,1, 0];//8
        this.charCodelookup[57] = [1,1,1,1, 1,1,0,1, 0,0,0,1, 0,0,0,1, 0];//9

        //CAPITOL ABC
        this.charCodelookup[65] = [1,1,1,1, 0,0,1,1, 0,0,0,1, 0,0,0,1, 0];//A
        this.charCodelookup[66] = [1,1,1,1, 1,1,0,0, 0,1,0,1, 0,1,0,0, this.w * -0.25];//B
        this.charCodelookup[67] = [1,1,0,0, 1,1,1,1, 0,0,0,0, 0,0,0,0, 0];//C
        this.charCodelookup[68] = [1,1,1,1, 1,1,0,0, 0,1,0,0, 0,1,0,0, this.w * -0.25];//D
        this.charCodelookup[69] = [1,1,0,0, 1,1,1,1, 0,0,0,1, 0,0,0,1, 0];//E
        this.charCodelookup[70] = [1,1,0,0, 0,0,1,1, 0,0,0,1, 0,0,0,1, 0];//F
        this.charCodelookup[71] = [1,1,0,1, 1,1,1,1, 0,0,0,1, 0,0,0,0, 0];//G
        this.charCodelookup[72] = [0,0,1,1, 0,0,1,1, 0,0,0,1, 0,0,0,1, 0];//H
        this.charCodelookup[73] = [1,1,0,0, 1,1,0,0, 0,1,0,0, 0,1,0,0, 0];//I
        this.charCodelookup[74] = [0,1,1,1, 1,1,1,0, 0,0,0,0, 0,0,0,0, 0];//J
        this.charCodelookup[75] = [0,0,0,0, 0,0,1,1, 0,0,1,0, 1,0,0,1, 0];//K
        this.charCodelookup[76] = [0,0,0,0, 1,1,1,1, 0,0,0,0, 0,0,0,0, 0];//L
        this.charCodelookup[77] = [0,0,1,1, 0,0,1,1, 1,0,1,0, 0,0,0,0, 0];//M
        this.charCodelookup[78] = [0,0,1,1, 0,0,1,1, 1,0,0,0, 1,0,0,0, 0];//N
        this.charCodelookup[79] = [1,1,1,1, 1,1,1,1, 0,0,0,0, 0,0,0,0, 0];//O
        this.charCodelookup[80] = [1,1,1,0, 0,0,1,1, 0,0,0,1, 0,0,0,1, 0];//P
        this.charCodelookup[81] = [1,1,1,1, 1,1,1,1, 0,0,0,0, 1,0,0,0, 0];//Q
        this.charCodelookup[82] = [1,1,1,0, 0,0,1,1, 0,0,0,1, 1,0,0,0, this.w * -0.25];//R
        this.charCodelookup[83] = [1,1,0,1, 1,1,0,1, 0,0,0,1, 0,0,0,1, 0];//S
        this.charCodelookup[84] = [1,1,0,0, 0,0,0,0, 0,1,0,0, 0,1,0,0, 0];//T
        this.charCodelookup[85] = [0,0,1,1, 1,1,1,1, 0,0,0,0, 0,0,0,0, 0];//U
        this.charCodelookup[86] = [0,0,0,0, 0,0,1,1, 0,0,1,0, 0,0,1,0, 0];//V
        this.charCodelookup[87] = [0,0,1,1, 1,1,1,1, 0,0,0,0, 0,1,0,0, 0];//W
        this.charCodelookup[88] = [0,0,0,0, 0,0,0,0, 1,0,1,0, 1,0,1,0, 0];//X
        this.charCodelookup[89] = [0,0,0,0, 0,0,0,0, 1,0,1,0, 0,1,0,0, 0];//Y
        this.charCodelookup[90] = [1,1,0,0, 1,1,0,0, 0,0,1,0, 0,0,1,0, 0];//Z

        this.charCodelookup[91] = [1,0,0,0, 0,1,1,1, 0,0,0,0, 0,0,0,0, 0];//[ 
        this.charCodelookup[93] = [0,1,1,1, 1,0,0,0, 0,0,0,0, 0,0,0,0, 0];//]

        this.charCodelookup[47] = [0,0,0,0, 0,0,0,0, 0,0,1,0, 0,0,1,0, 0];// /

    }

    writeWord( word, pos, scale, color, center = false, valign = false, spacing = 1.2 ){

        pos = pos.clone();

        if( word.length > 0 )
            word = word.toUpperCase();

        if( center ){
            pos.x -= (word.length - 0.5) * ( this.w*scale*spacing + 0.25 ) * 0.5;
        }

        if( valign ){
            pos.z += scale * 0.5;
        }

        for( var i = 0; i < word.length; i++ ){
            this.write( word.charCodeAt(i), pos, scale, color, 10 );
            pos.x += this.w*scale*spacing + 0.25;
        }

    }

    write( r, pos, scale, color ){

        //color = new THREE.Color( 0x00ff00 );

        var dot;

        if( r == 58 ){ // :

            //pos, height, width, color 
            dot = pos.clone();
            dot.x += this.w/2 * scale;
            this.geoBarn.drawFilledRect( dot, scale * 0.1, scale * 0.1, color );
            dot.z += this.h * scale;
            this.geoBarn.drawFilledRect( dot, scale * 0.1, scale * 0.1, color );

        }
        else if( r == 46 ){ // .

            //pos, height, width, color 
            dot = pos.clone();
            dot.x += this.w/2 * scale;
            this.geoBarn.drawFilledRect( dot, scale * 0.1, scale * 0.1, color );

        }
        else{

            var reg = this.charCodelookup[r];

            var p0 = this.p0.clone();
            var p1 = this.p1.clone();
            var p2 = this.p2.clone();
            var p3 = this.p3.clone();
            var p4 = this.p4.clone();
            var p5 = this.p5.clone();
            var p6 = this.p6.clone();
            var p7 = this.p7.clone();
            var p8 = this.p8.clone();

            var midOffset = reg[16];

            p1.x += midOffset;
            p8.x += midOffset;
            p5.x += midOffset;

            // var jitterx = 0.0;
            // var jitterz = 0.0;
            // p0.x += (Math.random() * 2.0 - 1.0) * jitterx;
            // p0.z += (Math.random() * 2.0 - 1.0) * jitterz;
            // p1.x += (Math.random() * 2.0 - 1.0) * jitterx;
            // p1.z += (Math.random() * 2.0 - 1.0) * jitterz;
            // p2.x += (Math.random() * 2.0 - 1.0) * jitterx;
            // p2.z += (Math.random() * 2.0 - 1.0) * jitterz;
            // p3.x += (Math.random() * 2.0 - 1.0) * jitterx;
            // p3.z += (Math.random() * 2.0 - 1.0) * jitterz;
            // p4.x += (Math.random() * 2.0 - 1.0) * jitterx;
            // p4.z += (Math.random() * 2.0 - 1.0) * jitterz;
            // p5.x += (Math.random() * 2.0 - 1.0) * jitterx;
            // p5.z += (Math.random() * 2.0 - 1.0) * jitterz;
            // p6.x += (Math.random() * 2.0 - 1.0) * jitterx;
            // p6.z += (Math.random() * 2.0 - 1.0) * jitterz;
            // p7.x += (Math.random() * 2.0 - 1.0) * jitterx;
            // p7.z += (Math.random() * 2.0 - 1.0) * jitterz;
            // p8.x += (Math.random() * 2.0 - 1.0) * jitterx;
            // p8.z += (Math.random() * 2.0 - 1.0) * jitterz;

            if(reg[0]){ this.addLineVertexFromPoints( p0, p1, pos, scale, color ); }
            if(reg[1]){ this.addLineVertexFromPoints( p1, p2, pos, scale, color ); }
            if(reg[2]){ this.addLineVertexFromPoints( p2, p3, pos, scale, color ); }
            if(reg[3]){ this.addLineVertexFromPoints( p3, p4, pos, scale, color ); }
            if(reg[4]){ this.addLineVertexFromPoints( p4, p5, pos, scale, color ); }
            if(reg[5]){ this.addLineVertexFromPoints( p5, p6, pos, scale, color ); }
            if(reg[6]){ this.addLineVertexFromPoints( p6, p7, pos, scale, color ); }
            if(reg[7]){ this.addLineVertexFromPoints( p7, p0, pos, scale, color ); }
            if(reg[8]){ this.addLineVertexFromPoints( p0, p8, pos, scale, color ); }
            if(reg[9]){ this.addLineVertexFromPoints( p1, p8, pos, scale, color ); }
            if(reg[10]){ this.addLineVertexFromPoints( p2, p8, pos, scale, color ); }
            if(reg[11]){ this.addLineVertexFromPoints( p3, p8, pos, scale, color ); }
            if(reg[12]){ this.addLineVertexFromPoints( p4, p8, pos, scale, color ); }
            if(reg[13]){ this.addLineVertexFromPoints( p5, p8, pos, scale, color ); }
            if(reg[14]){ this.addLineVertexFromPoints( p6, p8, pos, scale, color ); }
            if(reg[15]){ this.addLineVertexFromPoints( p7, p8, pos, scale, color ); }


        }

        // if( width > 0 ){

        //  let shiftPos = pos.clone().add( new THREE.Vector3(width,0,width * 2) );
        //  if(reg[0]){ this.addLineVertexFromPoints( p0, p1, shiftPos, scale, color ); }
        //  if(reg[1]){ this.addLineVertexFromPoints( p1, p2, shiftPos, scale, color ); }
        //  if(reg[2]){ this.addLineVertexFromPoints( p2, p3, shiftPos, scale, color ); }
        //  if(reg[3]){ this.addLineVertexFromPoints( p3, p4, shiftPos, scale, color ); }
        //  if(reg[4]){ this.addLineVertexFromPoints( p4, p5, shiftPos, scale, color ); }
        //  if(reg[5]){ this.addLineVertexFromPoints( p5, p6, shiftPos, scale, color ); }
        //  if(reg[6]){ this.addLineVertexFromPoints( p6, p7, shiftPos, scale, color ); }
        //  if(reg[7]){ this.addLineVertexFromPoints( p7, p0, shiftPos, scale, color ); }
        //  if(reg[8]){ this.addLineVertexFromPoints( p0, p8, shiftPos, scale, color ); }
        //  if(reg[9]){ this.addLineVertexFromPoints( p1, p8, shiftPos, scale, color ); }
        //  if(reg[10]){ this.addLineVertexFromPoints( p2, p8, shiftPos, scale, color ); }
        //  if(reg[11]){ this.addLineVertexFromPoints( p3, p8, shiftPos, scale, color ); }
        //  if(reg[12]){ this.addLineVertexFromPoints( p4, p8, shiftPos, scale, color ); }
        //  if(reg[13]){ this.addLineVertexFromPoints( p5, p8, shiftPos, scale, color ); }
        //  if(reg[14]){ this.addLineVertexFromPoints( p6, p8, shiftPos, scale, color ); }
        //  if(reg[15]){ this.addLineVertexFromPoints( p7, p8, shiftPos, scale, color ); }

        //  // geoBarn.addLine( p0, p0.clone().add( new THREE.Vector3(-width,0,width) ), color, color );
        //  // geoBarn.addLine( p1, p1.clone().add( new THREE.Vector3(-width,0,width) ), color, color );
        //  // geoBarn.addLine( p2, p2.clone().add( new THREE.Vector3(-width,0,width) ), color, color );
        //  // geoBarn.addLine( p3, p3.clone().add( new THREE.Vector3(-width,0,width) ), color, color );
        //  // geoBarn.addLine( p4, p4.clone().add( new THREE.Vector3(-width,0,width) ), color, color );
        //  // geoBarn.addLine( p5, p5.clone().add( new THREE.Vector3(-width,0,width) ), color, color );
        //  // geoBarn.addLine( p6, p6.clone().add( new THREE.Vector3(-width,0,width) ), color, color );
        //  // geoBarn.addLine( p7, p7.clone().add( new THREE.Vector3(-width,0,width) ), color, color );
        //  // geoBarn.addLine( p8, p8.clone().add( new THREE.Vector3(-width,0,width) ), color, color );

        // }
    

    }


    addLineVertexFromPoints( p1, p2, pos, scale, color ){

        var start = pos.clone().add(p1.clone().multiplyScalar(scale));
        var end = pos.clone().add(p2.clone().multiplyScalar(scale));

        this.geoBarn.addLine( start , end , color, color ); 


    }


}

module.exports = {
    GeoBarn,
    Seg16
};

