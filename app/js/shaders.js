const THREE = require('three')
  , EffectComposer = require('three-effectcomposer')(THREE);

class HyperBloomPass  {

    constructor( resolution, mips ){
        
        //THREE.Pass.call( this );

        this.resolution = ( resolution !== undefined ) ? new THREE.Vector2( resolution.x, resolution.y ) : new THREE.Vector2( 256, 256 );

        //Testing
        var testPars = { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter, format: THREE.RGBAFormat };

        this.renderTargetMips = [];
        this.nMips = mips;
        var resx = Math.ceil( this.resolution.x / 2 );
        var resy = Math.ceil( this.resolution.y / 2 );

        for ( var i = 0; i < this.nMips; i ++ ) {

            var renderTarget = new THREE.WebGLRenderTarget( resx, resy, testPars );
            renderTarget.texture.name = "HyperBloomPass.mip" + i;
            renderTarget.texture.generateMipmaps = false;
            renderTarget.texture.type = THREE.HalfFloatType;
            this.renderTargetMips.push( renderTarget );

            resx = Math.ceil( resx / 2 );
            resy = Math.ceil( resy / 2 );

        }

        this.inputTarget = new THREE.WebGLRenderTarget( this.resolution.x, this.resolution.y, testPars );
        this.inputTarget.texture.name = "HyperBloomPass.mip" + i;
        this.inputTarget.texture.generateMipmaps = false;
        this.inputTarget.texture.type = THREE.HalfFloatType;
        
        // Down Material

        this.upMaterials = [];
        this.downMaterials = [];

        resx = Math.ceil( this.resolution.x );
        resy = Math.ceil( this.resolution.y );

        for ( i = 0; i < this.nMips; i ++ ) {

            this.downMaterials.push( this.getDownMaterial() );
            this.upMaterials.push( this.getUpMaterial() );
            
            this.downMaterials[ i ].uniforms[ "hiSize" ].value = new THREE.Vector2( resx, resy );
            this.upMaterials[ i ].uniforms[ "hiSize" ].value = new THREE.Vector2( resx, resy );
            
            resx = Math.ceil( resx / 2 );
            resy = Math.ceil( resy / 2 );
            
            this.downMaterials[ i ].uniforms[ "loSize" ].value = new THREE.Vector2( resx, resy );
            this.upMaterials[ i ].uniforms[ "loSize" ].value = new THREE.Vector2( resx, resy );

            this.upMaterials[ i ].blending = THREE.AdditiveBlending;
            this.upMaterials[ i ].transparent = true;

        }

        this.inputMaterial = this.getInputMaterial();
        //Following needed?
        //this.getInputMaterial.blending = THREE.AdditiveBlending;
        //this.inputMaterial.transparent = false;

        this.outputMaterial = this.getOutputMaterial();
        this.outputMaterial.blending = THREE.AdditiveBlending;
        this.outputMaterial.transparent = true;

        this.enabled = true;
        this.needsSwap = false;

        this.oldClearColor = new THREE.Color();
        this.oldClearAlpha = 1;

        this.camera = new THREE.OrthographicCamera( - 1, 1, 1, - 1, 0, 1 );
        this.scene = new THREE.Scene();

        this.quad = new THREE.Mesh( new THREE.PlaneBufferGeometry( 2, 2 ), null );
        this.quad.frustumCulled = false; // Avoid getting clipped
        this.scene.add( this.quad );
    }
    
    dispose() {

        for ( var i = 0; i < this.renderTargetMips.length; i ++ ) {

            this.renderTargetMips[ i ].dispose();

        }

    }

    setSize( width, height ) {

        this.resolution.set( width, height ); 
        this.inputTarget.setSize( this.resolution.x, this.resolution.y );

        var resx = Math.ceil( width );
        var resy = Math.ceil( height );

        for ( var i = 0; i < this.nMips; i ++ ) {

            this.downMaterials[ i ].uniforms[ "hiSize" ].value = new THREE.Vector2( resx, resy );
            this.upMaterials[ i ].uniforms[ "hiSize" ].value = new THREE.Vector2( resx, resy );

            resx = Math.ceil( resx / 2 );
            resy = Math.ceil( resy / 2 );

            this.downMaterials[ i ].uniforms[ "loSize" ].value = new THREE.Vector2( resx, resy );
            this.upMaterials[ i ].uniforms[ "loSize" ].value = new THREE.Vector2( resx, resy );
            this.renderTargetMips[ i ].setSize( resx, resy );

        }

    }

    render( renderer, writeBuffer, readBuffer, delta, maskActive ) {

        this.oldClearColor.copy( renderer.getClearColor() );
        this.oldClearAlpha = renderer.getClearAlpha();
        var oldAutoClear = renderer.autoClear;
        renderer.autoClear = false;

        renderer.setClearColor( new THREE.Color( 0, 0, 0 ), 0 );

        if ( maskActive ) renderer.context.disable( renderer.context.STENCIL_TEST );

        // need to insert inputMaterial pass here
        this.quad.material = this.inputMaterial;
        this.inputMaterial.uniforms[ "tex" ].value = readBuffer.texture;
        renderer.render( this.scene, this.camera, this.inputTarget, false ); 

        var inputRenderTarget = this.inputTarget;

        for ( var i = 0; i < this.nMips; i++ ) {

            this.quad.material = this.downMaterials[ i ];
            this.downMaterials[ i ].uniforms[ "tex" ].value = inputRenderTarget.texture;
            renderer.render( this.scene, this.camera, this.renderTargetMips[ i ], false );
            inputRenderTarget = this.renderTargetMips[ i ];
        }

        // up material pass
        for ( i = this.nMips - 1; i > 0; i-- ) 
        {
            this.quad.material = this.upMaterials[ i ];
            this.upMaterials[ i ].uniforms[ "tex" ].value = inputRenderTarget.texture;
            renderer.render( this.scene, this.camera, this.renderTargetMips[ i - 1 ], false );
            inputRenderTarget = this.renderTargetMips[ i - 1 ];
        }

        // Blend it additively over the input texture
        this.quad.material = this.outputMaterial;
        this.outputMaterial.uniforms[ "tex" ].value = this.renderTargetMips[0].texture;
        //this.copyUniforms[ "tDiffuse" ].value = this.renderTargetMips[4].texture;

        if ( maskActive ) renderer.context.enable( renderer.context.STENCIL_TEST );

        renderer.render( this.scene, this.camera, readBuffer, false );

        renderer.setClearColor( this.oldClearColor, this.oldClearAlpha );
        renderer.autoClear = oldAutoClear;

    }

    getDownMaterial() {

        return new THREE.ShaderMaterial( {

            defines: {
                "NUM_MIPS": 1
            },

            uniforms: {
                "tex": { value: null },
                "loSize": { value: null },
                "hiSize": { value: null }
            },

            vertexShader:
                "varying vec2 vUv;\n\
                void main() {\n\
                    vUv = uv;\n\
                    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n\
                }",

            fragmentShader:
                "varying vec2 vUv;\
                uniform sampler2D tex;\
                uniform vec2 loSize;\
                uniform vec2 hiSize;\
                \
                void main() {\
                    vec2 uv = 2.0*loSize/hiSize*vUv;\
                    vec2 offset = vec2(0.668)/hiSize;\
                    gl_FragColor = vec4(0.0);\
                    gl_FragColor += texture2D(tex, uv + vec2(offset.x,offset.y));\
                    gl_FragColor += texture2D(tex, uv + vec2(-offset.x,offset.y));\
                    gl_FragColor += texture2D(tex, uv + vec2(offset.x,-offset.y));\
                    gl_FragColor += texture2D(tex, uv + vec2(-offset.x,-offset.y));\
                    gl_FragColor /= 4.0;\
                }"
        } );

    }

    getUpMaterial() {

        return new THREE.ShaderMaterial( {

            defines: {
                "NUM_MIPS": 1
            },

            uniforms: {
                "tex": { value: null },
                "loSize": { value: null },
                "hiSize": { value: null }
            },

            vertexShader:
                "varying vec2 vUv;\n\
                void main() {\n\
                    vUv = uv;\n\
                    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n\
                }",

            fragmentShader:
                "varying vec2 vUv;\
                uniform sampler2D tex;\
                uniform vec2 loSize;\
                uniform vec2 hiSize;\
                \
                void main() {\
                    vec2 uv = hiSize/(2.0*loSize)*vUv;\
                    \
                    vec2 offset = vec2(1.0)/hiSize;\
                    if( fract( floor(gl_FragCoord.x) / 2.0 ) != 0.0 ){ offset.x *= -1.0; }\
                    if( fract( floor(gl_FragCoord.y) / 2.0 ) != 0.0 ){ offset.y *= -1.0; }\
                    \
                    gl_FragColor = vec4(0.0);\
                    \
                    gl_FragColor += 0.09 * texture2D(tex, uv + vec2( -1.5, -1.5 ) * offset);\
                    gl_FragColor += 0.19 * texture2D(tex, uv + vec2( 0.5, -1.5 ) * offset);\
                    gl_FragColor += 0.02 * texture2D(tex, uv + vec2( 2.5, -1.5 ) * offset);\
                    \
                    gl_FragColor += 0.19 * texture2D(tex, uv + vec2( -1.5, 0.5 ) * offset);\
                    gl_FragColor += 0.42 * texture2D(tex, uv + vec2( 0.5, 0.5 ) * offset);\
                    gl_FragColor += 0.04 * texture2D(tex, uv + vec2( 2.5, 0.5 ) * offset);\
                    \
                    gl_FragColor += 0.02 * texture2D(tex, uv + vec2( -1.5, 2.5 ) * offset);\
                    gl_FragColor += 0.04 * texture2D(tex, uv + vec2( 0.5, 2.5 ) * offset);\
                    gl_FragColor += 0.0 * texture2D(tex, uv + vec2( 2.5, 2.5 ) * offset);\
                    \
                    \
                    gl_FragColor.a = 1.0;\
                }"
        } );

        //gl_FragColor.rgb *= 0.8;\

    }

    getInputMaterial() {

        return new THREE.ShaderMaterial( {

            defines: {
                "NUM_MIPS": 1
            },

            uniforms: {
                "tex": { value: null }
            },

            vertexShader:
                "varying vec2 vUv;\n\
                void main() {\n\
                    vUv = uv;\n\
                    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n\
                }",

            fragmentShader:
                "varying vec2 vUv;\
                uniform sampler2D tex;\
                \
                void main() {\
                    vec4 color = texture2D(tex, vUv);\
                    gl_FragColor = pow( color, vec4(2.2) );\
                }"
                
                //color = pow( color, vec4(2.2) );\
                //gl_FragColor = color * 0.25;\
        } );

    }

    getOutputMaterial() {

        return new THREE.ShaderMaterial( {

            defines: {
                "NUM_MIPS": 1
            },

            uniforms: {
                "tex": { value: null }
            },

            vertexShader:
                "varying vec2 vUv;\n\
                void main() {\n\
                    vUv = uv;\n\
                    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n\
                }",

            fragmentShader:
                "varying vec2 vUv;\
                uniform sampler2D tex;\
                \
                void main() {\
                    vec4 color = texture2D(tex, vUv);\
                    color = pow( color * 0.4, vec4(1.0/2.2) );\
                    color[0] = color[0] * (1.0 - 0.2126);\
                    color[1] = color[1] * (1.0 - 0.07152);\
                    color[2] = color[2] * (1.0 - 0.0722);\
                    \
                    gl_FragColor = color;\
                }"
                //gl_FragColor = pow( color * 0.8, vec4(1.0/2.2) );\
        } );

    }

}

module.exports = {
    HyperBloomPass
};