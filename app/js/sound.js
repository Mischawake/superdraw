
//require("@mohayonao/web-audio-api-shim");
const Pizzicato = require("pizzicato");
//const fetch = require("fetch").fetchUrl;

//let audioContext = new AudioContext();

const chord = ['F3','Ab3','C4','Db4','Eb4','F4','Ab4','C5','Db5','Eb5'];
const effectChord = ['F2','Ab2','C3','Db3','Eb3','F3'];


class SoundManager{

    constructor(){

        // var convolver = new Pizzicato.Effects.Convolver({
  //        impulse: './assets/AirportTerminal.wav',
  //        mix: 0.5
        // });

        this.reverb = new Pizzicato.Effects.Reverb({
            time: 8.00,
            decay: 4.00,
            reverse: false,
            mix: 0.5
        });
        
        this.on = true;

        this.loops = [];
        this.effects = [];

        for( let i = 0; i < chord.length; i++ ){

            let loop = new SoundLoop( 'Harp', chord[i], this.reverb, Math.random() * 8.0, 17.0 + Math.random() * 17.0 );
            this.loops.push( loop );

            //loop = new SoundLoop( 'Flute Stc', chord[i], reverb, Math.random() * 8.0, 17.0 + Math.random() * 17.0 );
            //this.loops.push( loop );
        }

        for( let i = 0; i < effectChord.length; i++ ){

            let loop = new SoundLoop( 'Flute Stc', effectChord[i], this.reverb, 0, 10.0 );
            this.effects.push( loop );

            //loop = new SoundLoop( 'Flute Stc', chord[i], reverb, Math.random() * 8.0, 17.0 + Math.random() * 17.0 );
            //this.loops.push( loop );
        }

    }

    toggleOn(){
        this.on = !this.on;
    }

    isOn(){
        return this.on;
    }

    update(dt){

        Pizzicato.volume = this.on ? 0.01 : 0.0;

        //if(!this.on) return;

        for( let i = 0; i < this.loops.length; i++ ){
            this.loops[i].update(dt);
        }

    }

    playRandomLoop(){

        let idx = Math.floor( Math.random()*this.loops.length );
        this.loops[ idx ].playNow(true);

    }

    playRandomEffect(){

        let idx = Math.floor( Math.random()*this.effects.length );
        this.effects[ idx ].playNow();

    }

}

class SoundLoop{

    constructor( instrument, note, effect, delay, duration ){
        let sample = getSample(instrument,note);
        this.rate = sample.rate;
        this.loaded = false;
        //console.log( sample.file );
        this.sound = new Pizzicato.Sound(encodeURIComponent(sample.file), function() {
            //sound loaded
            this.loaded = true;
            this.sound.addEffect( effect );
        }.bind(this));
        this.duration = duration;
        this.time = -delay;
    }

    playNow( resetLoop = false ){
        if( this.loaded ){
            this.sound.play();
            this.sound.sourceNode.playbackRate.value = this.rate;
        }
        if( resetLoop ){
            this.time = 0;
        }
    }

    update(dt){

        if( !this.loaded ) return;

        this.time += dt;
        if( this.time > this.duration ){
            this.playNow( true );
        }
    }
}

const SAMPLE_LIBRARY = {
    'Harp': [
        { note: 'A',  octave: 2, file: '/assets/harp/harp-a2.wav' },
        { note: 'A',  octave: 3, file: '/assets/harp/harp-a3.wav' },
        { note: 'A',  octave: 4, file: '/assets/harp/harp-a4.wav' },
        { note: 'A',  octave: 5, file: '/assets/harp/harp-a5.wav' },
        { note: 'A',  octave: 6, file: '/assets/harp/harp-a6.wav' },
        { note: 'C',  octave: 2, file: '/assets/harp/harp-c2.wav' },
        { note: 'C',  octave: 3, file: '/assets/harp/harp-c3.wav' },
        { note: 'C',  octave: 4, file: '/assets/harp/harp-c4.wav' },
        { note: 'C',  octave: 5, file: '/assets/harp/harp-c5.wav' },
        { note: 'C',  octave: 6, file: '/assets/harp/harp-c6.wav' },
        { note: 'C',  octave: 7, file: '/assets/harp/harp-c7.wav' },
        { note: 'D#',  octave: 2, file: '/assets/harp/harp-d#2.wav' },
        { note: 'D#',  octave: 3, file: '/assets/harp/harp-d#3.wav' },
        { note: 'D#',  octave: 4, file: '/assets/harp/harp-d#4.wav' },
        { note: 'D#',  octave: 5, file: '/assets/harp/harp-d#5.wav' },
        { note: 'D#',  octave: 6, file: '/assets/harp/harp-d#6.wav' },
        { note: 'F#',  octave: 2, file: '/assets/harp/harp-f#2.wav' },
        { note: 'F#',  octave: 3, file: '/assets/harp/harp-f#3.wav' },
        { note: 'F#',  octave: 4, file: '/assets/harp/harp-f#4.wav' },
        { note: 'F#',  octave: 5, file: '/assets/harp/harp-f#5.wav' },
        { note: 'F#',  octave: 6, file: '/assets/harp/harp-f#6.wav' },
    ],
    'Piano': [
        { note: 'A',  octave: 1, file: '/assets/piano/piano-p-a1.wav' },
        { note: 'A',  octave: 2, file: '/assets/piano/piano-p-a2.wav' },
        { note: 'A',  octave: 3, file: '/assets/piano/piano-p-a3.wav' },
        { note: 'A',  octave: 4, file: '/assets/piano/piano-p-a4.wav' },
        { note: 'A',  octave: 5, file: '/assets/piano/piano-p-a5.wav' },
        { note: 'A',  octave: 6, file: '/assets/piano/piano-p-a6.wav' },
        { note: 'A',  octave: 7, file: '/assets/piano/piano-p-a7.wav' },

        { note: 'C',  octave: 1, file: '/assets/piano/piano-p-c1.wav' },
        { note: 'C',  octave: 2, file: '/assets/piano/piano-p-c2.wav' },
        { note: 'C',  octave: 3, file: '/assets/piano/piano-p-c3.wav' },
        { note: 'C',  octave: 4, file: '/assets/piano/piano-p-c4.wav' },
        { note: 'C',  octave: 5, file: '/assets/piano/piano-p-c5.wav' },
        { note: 'C',  octave: 6, file: '/assets/piano/piano-p-c6.wav' },
        { note: 'C',  octave: 7, file: '/assets/piano/piano-p-c7.wav' },
        { note: 'C',  octave: 8, file: '/assets/piano/piano-p-c8.wav' },

        { note: 'D#',  octave: 1, file: '/assets/piano/piano-p-d#1.wav' },
        { note: 'D#',  octave: 2, file: '/assets/piano/piano-p-d#2.wav' },
        { note: 'D#',  octave: 3, file: '/assets/piano/piano-p-d#3.wav' },
        { note: 'D#',  octave: 4, file: '/assets/piano/piano-p-d#4.wav' },
        { note: 'D#',  octave: 5, file: '/assets/piano/piano-p-d#5.wav' },
        { note: 'D#',  octave: 6, file: '/assets/piano/piano-p-d#6.wav' },
        { note: 'D#',  octave: 7, file: '/assets/piano/piano-p-d#7.wav' },

        { note: 'F#',  octave: 1, file: '/assets/piano/piano-p-f#1.wav' },
        { note: 'F#',  octave: 2, file: '/assets/piano/piano-p-f#2.wav' },
        { note: 'F#',  octave: 3, file: '/assets/piano/piano-p-f#3.wav' },
        { note: 'F#',  octave: 4, file: '/assets/piano/piano-p-f#4.wav' },
        { note: 'F#',  octave: 5, file: '/assets/piano/piano-p-f#5.wav' },
        { note: 'F#',  octave: 6, file: '/assets/piano/piano-p-f#6.wav' },
        { note: 'F#',  octave: 7, file: '/assets/piano/piano-p-f#7.wav' },
    ],
    'Flute Stc': [
        { note: 'A',  octave: 3, file: '/assets/flute/flutes-stc-a3.wav' },
        { note: 'A',  octave: 4, file: '/assets/flute/flutes-stc-a4.wav' },
        { note: 'A',  octave: 5, file: '/assets/flute/flutes-stc-a5.wav' },
        { note: 'C',  octave: 3, file: '/assets/flute/flutes-stc-c3.wav' },
        { note: 'C',  octave: 4, file: '/assets/flute/flutes-stc-c4.wav' },
        { note: 'C',  octave: 5, file: '/assets/flute/flutes-stc-c5.wav' },
        { note: 'D#',  octave: 3, file: '/assets/flute/flutes-stc-d#3.wav' },
        { note: 'D#',  octave: 4, file: '/assets/flute/flutes-stc-d#4.wav' },
        { note: 'D#',  octave: 5, file: '/assets/flute/flutes-stc-d#5.wav' },
        { note: 'F#',  octave: 3, file: '/assets/flute/flutes-stc-f#3.wav' },
        { note: 'F#',  octave: 4, file: '/assets/flute/flutes-stc-f#4.wav' },
        { note: 'F#',  octave: 5, file: '/assets/flute/flutes-stc-f#5.wav' },
    ],
    'Horn': [
        { note: 'B',  octave: 3, file: '/assets/horn/cor_anglais-b3.wav' },
        { note: 'B',  octave: 4, file: '/assets/horn/cor_anglais-b4.wav' },
        { note: 'D',  octave: 4, file: '/assets/horn/cor_anglais-d4.wav' },
        { note: 'D',  octave: 5, file: '/assets/horn/cor_anglais-d5.wav' },
        { note: 'F',  octave: 3, file: '/assets/horn/cor_anglais-f3.wav' },
        { note: 'F',  octave: 4, file: '/assets/horn/cor_anglais-f4.wav' },
        { note: 'F',  octave: 5, file: '/assets/horn/cor_anglais-f5.wav' },
        { note: 'G#',  octave: 3, file: '/assets/horn/cor_anglais-g#3.wav' },
        { note: 'G#',  octave: 4, file: '/assets/horn/cor_anglais-g#4.wav' },
    ]
};

const OCTAVE = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'];

// function fetchSample(path) {
//  return fetch(encodeURIComponent(path))
//      .then(response => response.arrayBuffer())
//      .then(arrayBuffer => audioContext.decodeAudioData(arrayBuffer));
// }

function noteValue(note, octave) {
    return octave * 12 + OCTAVE.indexOf(note);
}

function getNoteDistance(note1, octave1, note2, octave2) {
    return noteValue(note1, octave1) - noteValue(note2, octave2);
}

function getNearestSample(sampleBank, note, octave) {
    let sortedBank = sampleBank.slice().sort((sampleA, sampleB) => {
        let distanceToA = Math.abs(getNoteDistance(note, octave, sampleA.note, sampleA.octave));
        let distanceToB = Math.abs(getNoteDistance(note, octave, sampleB.note, sampleB.octave));
        return distanceToA - distanceToB;
    });
    return sortedBank[0];
}

function flatToSharp(note) {
    switch (note) {
    case 'Bb': return 'A#';
    case 'Db': return 'C#';
    case 'Eb': return 'D#';
    case 'Gb': return 'F#';
    case 'Ab': return 'G#';
    default:   return note;
    }
}

function getSample(instrument, noteAndOctave) {
    let [, requestedNote, requestedOctave] = /^(\w[b\#]?)(\d)$/.exec(noteAndOctave);
    requestedOctave = parseInt(requestedOctave, 10);
    requestedNote = flatToSharp(requestedNote);
    let sampleBank = SAMPLE_LIBRARY[instrument];
    let sample = getNearestSample(sampleBank, requestedNote, requestedOctave);
    let distance = getNoteDistance(requestedNote, requestedOctave, sample.note, sample.octave);
    return {
        file: sample.file,
        rate: Math.pow(2, distance / 12)
    };
}


// function playSample(instrument, note, destination, delaySeconds = 0) {
//  getSample(instrument, note).then(({audioBuffer, distance}) => {
//      let playbackRate = Math.pow(2, distance / 12);

//      //console.log( "Rate:"+playbackRate );
//      let bufferSource = audioContext.createBufferSource();

//      bufferSource.buffer = audioBuffer;
//      bufferSource.playbackRate.value = playbackRate;

//      bufferSource.connect(destination);
//      bufferSource.start(audioContext.currentTime + delaySeconds);
//  });
// }

// function startLoop(instrument, note, destination, loopLengthSeconds, delaySeconds) {
//  playSample(instrument, note, destination, delaySeconds);
//      setInterval(
//      () => playSample(instrument, note, destination, delaySeconds),
//      loopLengthSeconds * 1000
//      );
// }

//mobile unlock?
if(Pizzicato.context.state === 'suspended' && 'ontouchstart' in window)
{
    var unlock = function()
    {
        Pizzicato.context.resume();
    };

    window.addEventListener('touchstart', unlock, false);
    window.addEventListener('touchend', unlock, false);
}

const soundManager = new SoundManager();
module.exports = {
    soundManager
};