const Key = Object.freeze({
    Space: 32,
    Left: 37,
    Up: 38,
    Right: 39,
    Down: 40,
    A: 65,
    D: 68,
    S: 83,
    W: 87,
    Mouse: 300,
    B: 66,
    C: 67,
    GT: 190,
    LT: 188
});

class Input {
    constructor() {
        this.keys = {};
        this.keysOld = {};
        this.touches = [];
        this.touchesOld = [];

        this.inputPos = {x:0,y:0};

        window.addEventListener('focus', e => this.onWindowFocus(e) , false);
        window.addEventListener('keydown', e => this.onKeyDown(e), false);
        window.addEventListener('keyup', e => this.onKeyUp(e), false);
        window.addEventListener('mousedown', e => this.onMouseDown(e), false);
        window.addEventListener('mouseup', e => this.onMouseUp(e), false);

        window.addEventListener("touchstart", e => this.onTouchStart(e), false);
        window.addEventListener("touchend", e => this.onTouchEnd(e), false);
        window.addEventListener("touchcancel", e => this.onTouchCancel(e), false);
        window.addEventListener("touchmove", e => this.onTouchMove(e), {passive: false} );

        document.addEventListener('mousemove', e => this.mouseMove(e), false );

        //window.addEventListener("deviceorientation", e => this.onOri(e), true);

    }

    touchByID(idToFind) {
        for (let i = 0; i < this.touches.length; i++) {
            let id = this.touches[i].identifier;
            if (id == idToFind) {
                return i;
            }
        }
        return -1;    // not found
    }

    onTouchStart(event) {

        let freshTouches = event.changedTouches;
        
        for (var i = 0; i < freshTouches.length; i++) {
            this.touches.push({ identifier: freshTouches[i].identifier });
        }

        this.inputPos.x = freshTouches[ freshTouches.length - 1 ].clientX;
        this.inputPos.y = freshTouches[ freshTouches.length - 1 ].clientY;

    }

    onTouchEnd(event) {

        let freshTouches = event.changedTouches;
        
        for (var i = 0; i < freshTouches.length; i++) {
         
            let idx = this.touchByID(freshTouches.identifier);
            this.touches.splice(idx,1);
        }
    }    

    onTouchCancel(event) {
   
    }

    onTouchMove(event) {

        let freshTouches = event.changedTouches;
        this.inputPos.x = freshTouches[ freshTouches.length - 1 ].clientX;
        this.inputPos.y = freshTouches[ freshTouches.length - 1 ].clientY;

        //prevents scrolling?
        event.preventDefault();
        event.stopPropagation();

    }

    touching() {
        return this.touches.length > 0;
    }

    onOri(e){

        var alpha = event.alpha;
        var beta = event.beta;
        var gamma = event.gamma;
    }

    mouseMove(e){

        this.inputPos.x = e.clientX;
        this.inputPos.y = e.clientY;
    }

    onMouseDown(event) {
        this.keys[300] = true;
    }

    onMouseUp(event) {
        delete this.keys[300];
    }

    onWindowFocus() {
        this.keys = {};
        this.keysOld = {};
    }

    onKeyDown(event) {
        this.keys[event.keyCode] = true;
    }

    onKeyUp(event) {
        delete this.keys[event.keyCode];
    }

    keyDown(key) {
        return this.keys[key] !== undefined;
    }

    keyPressed(key) {
        return this.keysOld[key] === undefined && this.keyDown(key);
    }

    keyReleased(key) {
        return this.keysOld[key] && !this.keyDown(key);
    }

    touchEnded(){
        return this.touchesOld.length > 0 && this.touches.length == 0;
    }

    flush() {
        this.keysOld = Object.assign({}, this.keys);
        this.touchesOld = Array.from(this.touches);
    }
}

const input = new Input();

module.exports = {
    input,
    Key
};