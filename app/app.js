const _ = require('lodash');
const _$ = require('jquery');
const now = require('performance-now');

//THREE.JS
const THREE = require('three')
    , EffectComposer = require('three-effectcomposer')(THREE); 

//Mr Doob Stats
const Stats = require('stats.js');

//My Engine Classes
const Shaders = require('./js/shaders');
const GeoBarn = require('./js/geo').GeoBarn; //weird naming workaround
const Seg16 = require('./js/geo').Seg16;
const Helpers = require('./js/helpers').Helpers;

//let PerlinGenerator = require("proc-noise");
//let Perlin = new PerlinGenerator();

//Globals
const DebugHud = require('./js/debug').debugHud;
const Input = require('./js/input').input;
const Key = require('./js/input').Key; //part of input
const SoundManager = require('./js/sound').soundManager;

let stats = new Stats(); // mrdoob stats
stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom

//Consts
const FRUSTUM = 25;
const FOG_DIST = 200.0;
const CULL_TIME = 1.0;
const PIXEL_TOLERANCE = 1.0;
const CAM_DIST = 50.0;


const GameMode = {
    Opening: 0,
    Playing: 1
};

let debugOpen = false;
//
// Game
//

let clearColor = new THREE.Color('black');
//let targetClearColor = new THREE.Color('black');

class Game {
    constructor(sendFn) {
        
        this.mode = GameMode.Opening;

        this.send = sendFn;
        this.playerId = 0;
        
        this.players = [];
        this.players.length = 2; //eventually set to max players..., maybe part of joined message set this?

        for( let i = 0; i < this.players.length; i++ )
        {
            this.players[i] = new Player(i);
        }

        this.timeSinceSend = 0;
        this.elapsedTime = 0;

        // //THREE JS
        this.renderer = new THREE.WebGLRenderer({
            antialias: true
        });

        this.canvas = this.renderer.domElement;//document.getElementById('canvas');

        let container = document.getElementById('webgl-container');
        container.appendChild(this.canvas);
    
        document.getElementById('stats').appendChild(stats.dom);

        this.ctx = this.canvas.getContext('2d');
        this.scene = new THREE.Scene();

        //fog
        this.scene.fog = new THREE.Fog( clearColor, CAM_DIST, CAM_DIST + FOG_DIST);
       
         // Perspective Camera
        this.camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 1, 2000);
        let aspect = window.innerWidth / window.innerHeight;
        this.camera.aspect = aspect;
        this.camera.rotation.x = -Math.PI / 2.0; //looking down
        this.camera.rotation.y = 0;
        this.camera.rotation.z = 0;
        this.scene.add(this.camera);

        //RENDERER
        this.renderer.setClearColor( clearColor );
        this.renderer.setPixelRatio(window.devicePixelRatio); //2 on retina
        //console.log( "Device Pixel Ratio " + window.devicePixelRatio + "H/W:" + window.innerWidth +","+ window.innerHeight );
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.shadowMap.enabled = false;
        //adjust how brights the brights are
        this.renderer.toneMapping = THREE.Uncharted2ToneMapping;
        this.renderer.toneMappingExposure = 2.0; 

        this.composer = new EffectComposer(this.renderer);
        let renderPass = new EffectComposer.RenderPass(this.scene, this.camera);
        this.bloomPass = new Shaders.HyperBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), 5);//1.0, 9, 0.5, 512);
        let copyShader = new EffectComposer.ShaderPass(EffectComposer.CopyShader);

        copyShader.renderToScreen = true;

        this.composer.addPass(renderPass);  
        this.composer.addPass(this.bloomPass);
        this.composer.addPass(copyShader);

        this.geoBarn = new GeoBarn( this.scene );
        this.seg16 = new Seg16( this.geoBarn );

        window.addEventListener('resize', e => this.resize(), false );

        //GAME SPECIFIC VARS

        this.inputPos = new THREE.Vector3();
        this.camera.position.set( 0, CAM_DIST, 0);
        this.cameraLean = new THREE.Vector3(0,0,0);
        this.pointArrays = [];
        this.pointer = 0;
        this.bonuses = [];
        this.kaleidescope = new Kaleidescope();

        _$('#soundbutton').click(() => {

            SoundManager.toggleOn();       
            _$('#soundbutton').text(SoundManager.isOn() ? "ON" : "OFF");

        });

        _$('#footer').click(() => {
            _$('#footer').addClass("open");
        });

        _$('#gameHolder').mouseup(() => {
            _$('#footer').removeClass("open");
        });

        this.resize();

    }


    update(dt, time, mode) {

        if( mode !== AppMode.Playing && mode !== AppMode.LostConn ) return;

        stats.begin();

        //clearColor.lerp( targetClearColor, dt );

        this.timeSinceSend += dt;
        this.elapsedTime += dt;
       
        this.players[ this.playerId ].active = true; // is this needed?

        //networking pulse
        if( this.timeSinceSend > 1.0 / 30.0 ){
            
            this.timeSinceSend = 0.0;
            this.broadcast();
          
        }

        let drawing = false;

        if( Input.keyDown(Key.Mouse) || Input.keyDown(Key.Space) || Input.touching() ){

            drawing = true;
            
        }

        if( Input.keyReleased(Key.D) ){
            debugOpen = !debugOpen;
            _$('#stats').toggle(debugOpen);
            _$('#debugHud').toggle(debugOpen);
        }

        //projection
        this.inputPos = Helpers.screenToWorldProjected( new THREE.Vector2( Input.inputPos.x, Input.inputPos.y ), this.camera );

        this.players[ this.playerId ].pos = this.inputPos;
        this.players[ this.playerId ].drawing = drawing;
    
        for( let i = 0; i < this.players.length; i++ ){

            this.players[i].update(dt, this.geoBarn, this.seg16, this.camera, this.kaleidescope );
        }

        this.kaleidescope.update(dt, this.geoBarn, this.elapsedTime, this.camera );

        this.scene.fog.far = this.kaleidescope.distance + Math.abs( this.camera.position.y );
        this.scene.fog.near = Math.abs( this.camera.position.y );

      
        this.scene.fog.color = clearColor;
        this.renderer.setClearColor( clearColor );

        let soundRate = (this.kaleidescope.speed / 10.0);

        SoundManager.update(dt * soundRate);
        //test circle
        //this.geoBarn.drawCircle( new THREE.Vector3(), 50.0, new THREE.Color('white') );

        //
        if( this.mode == GameMode.Opening ){

            if( Input.keyReleased(Key.Mouse) || Input.keyReleased(Key.Space) || Input.touchEnded() ){
                this.setMode( GameMode.Playing );
            }
        }

        for( let i = this.bonuses.length - 1; i >= 0; i--){
            this.bonuses[i].update(dt, this.geoBarn, this.seg16, i, 1.5);
            if( !this.bonuses[i].active ) this.bonuses.splice(i,1);
        }


        //do not modify below here...
        this.render();
        Input.flush();
        stats.end();
    }


    setMode(mode) {

        console.log("Game Mode: "+mode);
        if (this.mode != mode) {
            this.mode = mode;

            _$('#signin').toggle(mode == GameMode.Opening);

        }
    }

    clearBonuses(){
        this.bonuses.length = 0;
    }

    handleEffect( data ){

        if(this.elapsedTime > 1.0 ){
            SoundManager.playRandomEffect();
        }

    /*
    Types:
    Player Color 0
    Player Reflections 1
    */
        let type = data.bonus_type;

        switch (type) {
        case 0:
            // player color
            for( let i = 0; i<this.players.length; i++ ){
                if( data.playerId == this.players[i].id ){
                    this.players[i].targetHue = data.bonus_value;
                    break;
                }
            }

            break;
        case 1:
            // player reflections
            for( let i = 0; i<this.players.length; i++ ){
                if( data.playerId == this.players[i].id ){
                    this.players[i].reflects = data.bonus_value;
                    break;
                }
            }
            break;
        default:
            console.log('Unknown Effect');
        }

    }


    handleMood( data ){

        if(this.elapsedTime > 1.0 ){
            SoundManager.playRandomEffect();
            SoundManager.playRandomEffect();
        }

        this.kaleidescope.targetSpeed = data.speed;
        this.kaleidescope.targetSpin = data.spin;
        this.kaleidescope.targetDuration = data.duration;

        //set clear color to be triad with player colors
        //targetClearColor.setHSL(Math.random(),1.0,0.1);
    }

    handleBonus( data ){

        if( data.create ){
            //new bonus
            let b = new Bonus(data);
            this.bonuses.push( b );
        }
        else{
            //destroy
            for(let i = 0; i < this.bonuses.length; i++ ){

                if( this.bonuses[i].id == data.id ){
                    this.bonuses[i].killed = true;
                    this.bonuses[i].collected = data.activated > -1;
                }
            }
            
        }
        
    }

    broadcast(){

        //just need to send player pos, and drawing or not

        let arrayBuffer = new ArrayBuffer( 4 ); 
        let dataView = new DataView(arrayBuffer);
        let v = this.players[ this.playerId ].serializer();
        dataView.setUint32(0, v ); 
        this.send( 'binary', arrayBuffer );

    }

    render(){

        this.geoBarn.flush();
        this.composer.render();
        DebugHud.flush();

    }

    deserialize( data ){

        //deactivate all
        for( let i = 0; i < this.players.length; i++ ){
            this.players[i].active = false;
        } 

        let dataView = new DataView(data); 
        let offset = 0;
    
        let numPlayers = dataView.getUint16( offset );
        offset += 2;

        for( let i = 0; i < numPlayers; i++ ){

            let id = dataView.getUint16( offset );
            offset += 2;

            this.players[ id ].active = true;

            if( this.playerId != id ){
                let data = dataView.getUint32( offset );
                this.players[ id ].deserializer( data );
            }
            offset += 4;
            
        } 

    }

 
    resize(){

        let aspect = window.innerWidth / window.innerHeight;
        this.camera.aspect = aspect;

        if( aspect < 1 ){
            this.camera.position.y = CAM_DIST / aspect; 

        }

        this.renderer.setSize( window.innerWidth, window.innerHeight );
        this.composer.setSize( window.innerWidth, window.innerHeight ); 
        
        this.camera.updateProjectionMatrix();

    }

    outputImage()
    {

        console.log("Outputting Image");
        //console.log( this.renderer.domElement );
        // let myImage = this.renderer.domElement.toDataURL("image/png").replace("image/png", "image/octet-stream");
        // window.location.href=myImage;

        var download = document.getElementById("download");
        var image = this.canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
        document.getElementById("download").setAttribute("href", image);
              //download.setAttribute("download","archive.png");

      // var canvas1 = document.getElementById("canvasSignature");        
      // if (canvas1.getContext) {
      //    var ctx = canvas1.getContext("2d");                
      //    var myImage = canvas1.toDataURL("image/png");      
      // }
      // var imageElement = document.getElementById("MyPix");  
      // imageElement.src = myImage;                           

    }  



}

class Bonus{

    constructor(data){
        this.position = data.position;
        this.id = data.id;
        this.bonus_value = data.bonus_value;
        this.bonus_type = data.bonus_type;
        this.killed = false;
        this.duration = data.duration;
        this.lifetime = 0;
        this.active = true;
        this.colllected = false;
    }

    update( dt, geoBarn, seg16, idx, scale ){

        if( !this.active ) return;

        this.lifetime += dt;
        //console.log( this.position );

        if( this.killed ){
            this.lifetime = Math.max( this.duration, this.lifetime );
        }
        if( this.lifetime >= this.duration && !this.killed ){
            this.killed = true;
        }

        if( this.lifetime > this.duration + 2.0 ){ this.active = false;}

        let p = new THREE.Vector3( this.position.x, 0, -this.position.y);

        scale = scale * Math.min( Math.pow(this.lifetime,0.1), 1.0 );
  
        {

            switch (this.bonus_type) {
            case 0: {
                // player color
                let c = new THREE.Color();
                c.setHSL( this.bonus_value, 1.0, 0.3 );

                if( this.killed && this.collected ){
                    //popped
                    let rad = (this.lifetime - this.duration) * 100.0 + scale * 2.0;
                    let circum = Math.PI * 2 * rad;
                    geoBarn.drawCircle( p, rad, c, Math.floor( circum ) );
                }
                else if( this.killed ){
                    let rad = (scale * 2.0) * (1.0 - Math.min(this.lifetime - this.duration,1.0));
                    geoBarn.drawCircle( p, rad, c );
                }
                else{
                    //normal
                    geoBarn.drawFilledCircle( p, 2.0 * scale, clearColor );
                    geoBarn.drawCircle( p, 2.0 * scale, c );
                    geoBarn.drawFilledCircle( p, 0.75 * scale, c );
                }
                break;
            }
            case 1: {
                // player reflections
                let color = new THREE.Color(0x593D66);

                if( this.killed && this.collected ){
                    //popped
                    let rad = (this.lifetime - this.duration) * 100.0 + scale * 2.2;
                    //let circum = Math.PI * 2 * rad;
                    geoBarn.drawCircle( p, rad, color, 4 );
                }
                else if( this.killed ){
                    let rad = (scale * 2.2) * (1.0 - Math.min(this.lifetime - this.duration,1.0));
                    geoBarn.drawCircle( p, rad, color, 4 );
                }
                else{
                    //normal
                    geoBarn.drawFilledCircle( p, 2.2 * scale, clearColor, 4 );
                    geoBarn.drawCircle( p, 2.2 * scale, color, 4 );
                    let text = this.bonus_value + "";
                    if( this.bonus_value < 10 ) text = "0"+text; 
                    seg16.writeWord( text, p, 1.2 * scale, color, true, true ); 
                }
                break;
            }
            case 2:{
                //world translation
                let c = new THREE.Color(0x555555);
                let color_cold = new THREE.Color( 0x3D8FFF ).multiplyScalar(0.5);
                let color_heat = new THREE.Color( 0xff0427 );

                if( this.killed && this.collected ){
                    //popped
                    let rad = (this.lifetime - this.duration) * 100.0 + scale * 2.2;
                    geoBarn.drawCircle( p, rad, color_heat, 6, globalTime );
                }
                else{
                    if( this.killed ){
                        scale = (1.0 - Math.min(this.lifetime - this.duration,1.0));
                    }
            
                    //normal
                    geoBarn.drawCircle( p, 2.2 * scale, color_heat, 3, globalTime);
                    if( !this.killed ) geoBarn.drawFilledCircle( p, 2.2 * scale, color_heat.clone().multiplyScalar(0.05), 3, globalTime);

                    p.y += 0.1;
                    geoBarn.drawCircle( p, 2.2 * scale, color_cold, 3, Math.PI + globalTime * 1.0);
                    if( !this.killed ) geoBarn.drawFilledCircle( p, 2.2 * scale, color_cold.clone().multiplyScalar(0.05), 3, Math.PI + globalTime);
                    
                    p.y += 0.1;
                    if( !this.killed ) geoBarn.drawFilledCircle( p, 1.2 * scale, color_heat.clone().multiplyScalar(0.05), 3, globalTime);
                    geoBarn.drawCircle( p, 1.1 * scale, color_heat, 3, globalTime, true);
                }

            
                
                break;
            }
            default:
                console.log('Unknown Effect');
            }
        }

    

    }

    drawIndicator(p,geoBarn,v, scale, color = new THREE.Color('white')){
        
        if( v > 0){
            p.z -= 4.0 * scale;
            geoBarn.drawArc( p, scale, color, Math.PI * 5/2, Math.PI * 7/2, 4 ); // up
        } 
        else{
            p.z += 4.0 * scale;
            geoBarn.drawArc( p, scale, color, -Math.PI * 0.5, Math.PI * 0.5, 4 ); // down
        } 
        

    }

}




class Player{

    constructor(id){


        this.targetHue = Math.random();
        
        this.color = new THREE.Color('white');
        this.color.setHSL(this.targetHue, 1.0, 0.25 );
        this.color2 = this.color.clone();
        this.color2.offsetHSL( 0.1, 0, 0 );

        this.pos = new THREE.Vector3(0,0,0);
        this.stylusPos = new THREE.Vector3(0,0,0);
        this.drawing = false;
        this.id = id;
        this.active = false;
        this.curArray = null;

        this.colorTrack = 0;
        this.reflects = 1;

        this.drawTime = 0.0;

    }


    update(dt, geoBarn, seg16, camera, kaleidescope ){

        this.pos.y = 0;

        this.reflects = Math.max( this.reflects, 1 );
        this.reflects = Math.min( this.reflects, 100 );

        this.colorTrack += dt / 1.0;

        let targetColor = new THREE.Color('white');
        targetColor.setHSL(this.targetHue, 1.0, 0.25 );
        this.color.lerp( targetColor, dt );
        this.color2 = this.color.clone();
        this.color2.offsetHSL( 10.0/360.0, 0, 0 );

        let currentColor = this.color.clone().lerp( this.color2, (Math.sin(this.colorTrack) + 1.0)/2.0 );

        this.stylusPos.lerp( this.pos, dt * 10.0 );

        if( !this.active ) return;

        DebugHud.addLine( "Player" + this.id + " is active" );
    
        if( this.drawing ){
            geoBarn.drawFilledCircle( this.pos, 1.0, currentColor, 20 );
        }
        else{
            geoBarn.drawCircle( this.pos, 1.0, currentColor, 20 );
        }

        if( this.drawing && this.curArray == null ){
            this.stylusPos.copy( this.pos );
            this.curArray = kaleidescope.startLine( this.stylusPos.clone(), currentColor, this.reflects );
            //console.log("starting line");
        }
        else if( this.drawing ){
            kaleidescope.drawLine( this.curArray, this.stylusPos.clone(), currentColor, this.reflects );
        }
        else if( this.curArray !== null ){
            //finish line?
            if( this.drawTime > 0.1 ){
                SoundManager.playRandomLoop();
            }

            kaleidescope.drawLine( this.curArray, this.stylusPos.clone(), currentColor, this.reflects);
            this.curArray = null;
        }

        this.drawTime = this.drawing ? this.drawTime + dt : 0;

    }

    serializer(){

        let xCompacted = Helpers.compact( this.pos.x, -100, 100, 0xFFF ); //15 bits
        let zCompacted = Helpers.compact( this.pos.z, -100, 100, 0xFFF ); //15 bits

        let drawing = this.drawing ? 1 : 0;

        let v = 0;
        v = v | ( xCompacted & 0xFFF ) << 20;
        v = v | ( zCompacted & 0xFFF ) << 8;
        v = v | ( this.reflects & 0x7F ) << 1;
        v = v | ( drawing & 0x1 );

        return v;
    }

    deserializer(v){

        this.drawing = ( v & 0x1 ) === 1;
        //this.reflects = (v & 0xFE ) >>> 1;
        this.pos.z = Helpers.decompact((v & 0xFFF00 ) >>> 8, -100, 100, 0xFFF );
        this.pos.x = Helpers.decompact(( v & 0xFFF00000 ) >>> 20, -100, 100, 0xFFF);

    }

}

class Kaleidescope{

    constructor(){
        this.pointArrays = [];
        //this.pointPointer = 0;
        //this.activeIndex = Set();
        this.clearable = true;
        this.cullTimer = 0.0;

        //paramaters
        this.distance = 10.0;
        this.spin = 0.0;
        this.speed = 2.0;
        this.duration = 10.0;

        this.targetSpin = this.spin;
        this.targetSpeed = this.speed;
        this.targetDuration = this.duration;

        console.log( "Kaleidescope Init");

    }


    startLine(pos,color,reflects = 1){

        this.clearable = false;
        let d = [];
        d.push( [ pos.clone(), color, reflects ] );
        d.push( [ pos.clone(), color, reflects ] );//starting line needs two points
        this.pointArrays.push( d );
        //return current index
        return this.pointArrays.length-1;
    }

    drawLine( curArrayIdx, pos, color, reflects = 1, tolerance = 0.1){

        this.clearable = false;
        let curArray = this.pointArrays[curArrayIdx];

        if( typeof curArray == 'undefined') return; //hack to catch a bug...

        //let lastPoint = curArray[ curArray.length - 1 ][0];

        curArray[ curArray.length - 1 ] = [pos.clone(), color, reflects]; //set last point to current state

        let pu = curArray[ curArray.length - 2 ][0];

        if( pu.distanceTo( pos ) >= tolerance ){
           
            //if previous point falls on a line between new point and 2 points ago remove previous point?

            if( curArray.length >= 3 ){

                let a = curArray[ curArray.length - 3][0]; // 2 points ago
                let b = curArray[ curArray.length - 1][0]; // newest point
                
                let p = curArray[ curArray.length - 2][0]; // mid point

                //console.log( Helpers.distanceToLine(a, b, p ) );
                //a,b,p

                if( Helpers.distanceToLine(a, b, p ) < tolerance * 0.666 ){
                    curArray.splice( curArray.length - 2, 1 ); // midpoint
                }
            }

            curArray.push( [ pos.clone(), color, reflects ] );

        }

    }

    update(dt, geoBarn, elapsedTime, camera ){

        let pointCount = 0;

        // this.targetDrain = Helpers.clamp( this.targetDrain, -3.0, 3.0 );
        // this.targetTranslate = Helpers.clamp( this.targetTranslate, 2, 100 );
        // this.targetDuration = Helpers.clamp( this.targetDuration, 1.0, 30.0 );

        //lerp values
        let rate = dt * 0.5;
        this.spin = Helpers.lerp( rate, this.spin, this.targetSpin );
        this.speed = Helpers.lerp( rate, this.speed, this.targetSpeed );
        this.duration = Helpers.lerp( rate, this.duration, this.targetDuration );
        //automatic:

        DebugHud.addLine("Spin:"+this.spin.toFixed(2) );
        DebugHud.addLine("Speed:"+this.speed.toFixed(2) );
        DebugHud.addLine("Duration:"+this.duration.toFixed(2) );

        this.distance = Math.abs(this.speed * this.duration);

        this.cullTimer += dt;

        //draw newest first (that's why for loop is reversed) also allows splice
        for(let i = this.pointArrays.length - 1; i >= 0; i-- ){

            if( this.clearable && this.pointArrays[i].length <= 1 ){
                //cull lines with 1 point or less
                this.pointArrays.splice(i,1);
                continue;
                
            }

            let curArray = this.pointArrays[i];

            //update positions first
            for(let j = curArray.length - 1; j >= 0; j-- ){

                let p = curArray[j][0];

                p.y += dt * -this.speed;
                p.applyAxisAngle( new THREE.Vector3(0,1,0), -Math.abs(p.y/this.distance) * dt * this.spin );

                if( p.y <= -this.distance && j > 0 && this.clearable ){
                    curArray.splice(j - 1, 1);
                }
                
                if( j > 0 && this.clearable && this.cullTimer > CULL_TIME ){
                    if( Helpers.worldToScreenspace(p,camera).distanceToSquared( Helpers.worldToScreenspace(curArray[j-1][0],camera) ) < PIXEL_TOLERANCE ){
                        curArray.splice(j,1);
                    }
                }
                
            }



            if( curArray.length < 2 ) continue; //can't draw less than 2 points

            //then draw
            for(let j = 0; j < curArray.length; j++ ){

                if( j + 1 < curArray.length ){

                    let ref = curArray[j+1][2];

                    let p0 = curArray[j][0];
                    let p1 = curArray[j+1][0];
                    let c0 = curArray[j][1];
                    let c1 = curArray[j+1][1];

                    if( ref > 1 ){
                        geoBarn.addLineK( p0, p1, c0, c1, ref );
                    }
                    else
                    {
                        geoBarn.addLine( p0, p1, c0, c1 );
                    }
                    
                }

                pointCount++;

            }
        }

        if( this.cullTimer > CULL_TIME ){
            this.cullTimer = 0;
        }

        //DebugHud.addLine("Points:"+pointCount);
        this.clearable = true;
    }


}


//
// App
//
const AppMode = {
    None: 0,
    Title: 1,
    Waiting: 2,
    Playing: 3,
    LostConn: 4
};

let timeout = 0;

let globalTime = 0;

class App {
    constructor() {

        this.game = new Game((t, d) => {
            this.sendMessage(t, d);
        });

        // _$('#startBtn').click(() => {
        //     this.connect();
        // });

        this.mode = AppMode.None;
        this.setMode(AppMode.Title);

        this.connect();

        this.prevTime = 0;
        this.update();
        this.empty = false;
    }

    connect() {
        var host = window.document.location.host;
        
        if( host == 'localhost:8080' ){
            //TESTINGS
            this.ws = new WebSocket('ws://' + host + '/play');
        }
        else{
            //HTTPS, use secure ws
            this.ws = new WebSocket('wss://' + host + '/play'); 
        }

        this.ws.binaryType = "arraybuffer";

        this.ws.onerror = (error) => {
            console.log('socket error', error);
        };

        this.ws.onopen = () => {
            this.setMode(AppMode.Playing);
            console.log('Sending join');
            this.sendMessage('ready', {});
        };

        this.ws.onmessage = (e) => {

            let decode = function(data) {
                if (typeof(data) === 'object' ){
                    return {
                        type: 'binary',
                        data: data
                    };
                }
                else if (typeof(data) === 'string') {
                    return JSON.parse(data);
                } else {
                    return {
                        type: 'unknown type: ' + typeof(data),
                        data: data
                    };
                }
            };

            let msg = decode(e.data);
            this.onMessage(msg.type, msg.data);
        };

        this.ws.onclose = () => {
            console.log('socket closed');
            this.setMode(AppMode.LostConn);
        };
    }

    sendMessage(type, data) {

        if( this.mode == AppMode.LostConn ){
            return;
        } 

        let msg;
        if( type === "binary" ){
            msg = data;
        }
        else{
            msg = JSON.stringify({
                type: type,
                data: data
            });
        }
        this.ws.send(msg, (err) => {
            if (err !== undefined) {
                console.log('Send error', err);
            }
        });
    }

    onMessage(type, data) {
        switch (type) {
        case 'joined':
            console.log('Connected as player', data.playerId);
            this.game.playerId = data.playerId;
            this.game.clearBonuses();
            this.setMode(AppMode.Playing);
            break;
        case 'bonus':
            this.game.handleBonus( data );
            break;
        case 'status':
            _$("#global_players").text(data.currentPlayers);
            this.empty = data.currentPlayers <= 1;
            if( this.mode == AppMode.Playing){
                _$('#lonely').toggle(this.empty && this.game.elapsedTime > 5.0);
            }
            break;
        case 'effect':
            this.game.handleEffect( data );
            break;
        case 'mood':
            this.game.handleMood( data );
            break;
        case 'binary':
            this.game.deserialize( data );
            break;
        default:
            console.log('Unknown message', type, data);
        }
    }

    update() {

        let time = now();
        let dt = (time - this.prevTime) / 1000.0;
        this.prevTime = time;
        requestAnimationFrame((t) => this.update(t));

        this.game.update(dt, time, this.mode);
        globalTime += dt;

        if( this.mode == AppMode.LostConn ){
            //console.log( "Reconnecting in "+(5.0 - timeout));
            timeout += dt;
            if( timeout > 5.0 ){
                this.connect();
                timeout = 0;
            }

        }



    }

    setMode(mode) {

        //console.log("Mode: "+mode);
        if (this.mode != mode) {
            this.mode = mode;


            if( mode == AppMode.LostConn ){ 
                _$("#global_players").text("Lost Connection");
                this.game.clearBonuses();
            } 
            //_$('#splashHolder').toggle(mode == AppMode.Title);


 
        }
    }
}

function run() {

    let app = new App();
    //app.connect();
    DebugHud.container =  document.getElementById("debugHud");
}


window.addEventListener('load', e => { 
    run(); 

    //hacks to change controls for device
    var supportsTouch = false;
    if ('ontouchstart' in window) // iOS & android
        supportsTouch = true;
    else if(window.navigator.msPointerEnabled) // Win8
        supportsTouch = true;
    else if ('ontouchstart' in document.documentElement) // Controversal way to check touch support
        supportsTouch = true;

    _$('#desktop-control').toggle(!supportsTouch);
    _$('#mobile-control').toggle(supportsTouch);

});



