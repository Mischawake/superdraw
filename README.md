## Install

```console
$ npm install
```

## Running

```console
$ npm start
```

Visit http://localhost:8080

##Updating the data
you want to replace data/story.js
with the exported file
and you need to add the line
module.exports = { Content: storyContent };
to the end of it
after you export from ink

## Custom Ink Formatting
Use "@1" or "@2" before any text to make it only appear to player 1 or player 2
Use "#player: 1" before a choice set to set the player recieving the choices manually.

## SSH
you can view the log output by doing
sudo journalctl -f -u io
Restart io
sudo systemctl restart io

you can also stop the app
sudo systemctl stop io
and then run it yourself
and then sudo systemctl start io 
to run the daemon again

deploy/deploy.sh XXX.XX.XXX.XXX 
ssh -i ~/Developer/three.io/deploy/auth/machine -p 5022 mrio@XXX.XX.XXX.XX

## Hot reloading
Changing any *.js file will automatically refresh the webpage.